const router = require('express').Router()
const routerMaterias = require('./routes/materias')

router.get('/', (req, res) => {
  res.json({ mensaje: 'prueba' })
})

router.use('/', routerMaterias)

module.exports = router
