// para establecer las distintas rutas, necesitamos instanciar el express router
const router = require('express').Router()
const materiaController = require('../controllers/materiaController')
const moment = require('moment')

const auth = function (req, res, next) {
  console.log(req.headers.authorization)
  if (!req.headers.authorization) {
    return res.status(403).send({ message: 'No tienes permiso' })
  }
  const token = req.headers.authorization.split(' ')[1]

  try {
    payload = servicejwt.decodeToken(token)
  } catch (error) {
    return res.status(401).send(`${error}`)
  }
  res.status(200).send({ message: 'con permiso' })
}

router.get('/', (req, res) => {
  materiaController.index(req, res)
  res.json({ mensaje: 'prueba' })
})

router.get('/:id', (req, res) => {
  materiaController.show(req, res)
})

router.post('/', (req, res) => {
  materiaController.create(req, res)
})

router.put('/:id', (req, res) => {
  materiaController.update(req, res)
})

router.delete('/:id', (req, res) => {
  materiaController.destroy(req, res)
})

router.get('/privada', auth, (req, res) => {
  materiaController.index(req, res)
})

module.exports = router
